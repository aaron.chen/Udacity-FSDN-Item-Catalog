# Udacity-FSDN-Item-Catalog

Implementation of the "Item Catalog" project for Udacity's Full Stack Web Developer Nanodegree program. This project includes files needed to get a simple Flask server up and running that will serve out the Item Catalog web pages. In addition to looking at items, logged in users are able to add items, and edit/delete items if they were the original owner.

Authentication is done through Google Oauth.

## Setup

### VM

First, get the vagrant setup code for the FSDN course:

```bash
git clone https://github.com/udacity/fullstack-nanodegree-vm.git
```

Run `vagrant up` and `vagrant ssh` inside the `vagrant` folder of the VM after following the above instructions.

To do this (provided you got the `fullstack-nanodegree-vm` folder from cloning the git repository):
```bash
cd fullstack-nanodegree-vm/vagrant
vagrant up && vagrant ssh
```

### Transfer this code to the VM

Once you are ssh'd in, navigate to `/vagrant`. If there is already a `catalog` folder in `/vagrant`, remove it.

Once it is removed, run the following command:

```bash
git clone https://gitlab.com/aaron.chen/Udacity-FSDN-Item-Catalog.git catalog
```

### Setup the database

While ssh'd in, run the following commands:

```bash
cd /vagrant/catalog
python2 lotsofitems.py
```

Setup should now be complete.

## Usage

While ssh'd into the VM, run the following commands to start up the flask server:
```bash
cd /vagrant/catalog
python2 project.py
```

In a web browser, you can access the page at `localhost:8000`.

## API

### Catalog

```http
/catalog/json GET
```

Returns a JSON object containing all items in the catalog.

Example:
```json
[
  {
    "category_id": 1, 
    "description": "It's the ball you use to play soccer", 
    "id": 1, 
    "name": "Soccer Ball", 
    "user_id": 1
  }, 
  {
    "category_id": 1, 
    "description": "It's the net you have to get the soccer ball in to score a point", 
    "id": 2, 
    "name": "Soccer Goal", 
    "user_id": 1
  }
]
```
### Category

```http
/category/<int:category_id>/json GET
```

Returns a JSON object containing metadata about a category.

Example:
```json
{
  "id": 1, 
  "name": "Soccer"
}
```

### Item

```http
/item/<int:item_id>/json GET
```

Returns a JSON object containing information about a particular item.

Example:
```json
{
  "category_id": 1, 
  "description": "It's the ball you use to play soccer", 
  "id": 1, 
  "name": "Soccer Ball", 
  "user_id": 1
}
```