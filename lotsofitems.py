#!/usr/bin/env python2

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from database_setup import Base, User, Category, Item

engine = create_engine('sqlite:///items.db')
# Bind the engine to the metadata of the Base class so that the
# declaratives can be accessed through a DBSession instance
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
# A DBSession() instance establishes all conversations with the database
# and represents a "staging zone" for all the objects loaded into the
# database session object. Any change made against the objects in the
# session won't be persisted into the database until you call
# session.commit(). If you're not happy about the changes, you can
# revert all of them back to the last commit by calling
# session.rollback()
session = DBSession()

# Add user
user1 = User(name='Aaron', email='ohgeedubs@gmail.com',
             picture='https://cdn.bulbagarden.net/upload/2/28/704Goomy.png')
session.add(user1)
session.commit()

# add categories
soccer = Category(name='Soccer')
basketball = Category(name='Basketball')
baseball = Category(name='Baseball')
frisbee = Category(name='Frisbee')
snowboarding = Category(name='Snowboarding')
rock_climbing = Category(name='Rock Climbing')
foosball = Category(name='Foosball')
skating = Category(name='Skating')
hockey = Category(name='Hockey')

session.add(soccer)
session.add(basketball)
session.add(baseball)
session.add(frisbee)
session.add(snowboarding)
session.add(rock_climbing)
session.add(foosball)
session.add(skating)
session.add(hockey)

session.commit()

# Add soccer items
soccer_item1 = Item(
    name="Soccer Ball",
    description="It's the ball you use to play soccer",
    category=soccer,
    user=user1)
soccer_item2 = Item(name="Soccer Goal",
                    description="It's the net you have to get" +
                    "the soccer ball in to score a point",
                    category=soccer,
                    user=user1)

session.add(soccer_item1)
session.add(soccer_item2)
session.commit()

print "added items!"
