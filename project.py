#!/usr/bin/env python2

from database_setup import Base, Category, Item, User
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from flask import Flask, render_template, url_for
from flask import request, redirect, jsonify, flash
from flask import session as login_session
from flask import make_response
from oauth2client.client import flow_from_clientsecrets
from oauth2client.client import FlowExchangeError
import httplib2
import json
import requests
import random
import string

CLIENT_ID = json.loads(open('client_secrets.json', 'r').read())[
    'web']['client_id']

app = Flask(__name__)

# create session with database
engine = create_engine('sqlite:///items.db')
Base.metadata.bind = engine

DBsession = sessionmaker(bind=engine)
session = DBsession()


@app.route('/')
def renderDefaultPage():
    '''renders the home page'''
    homepage_url = url_for('renderDefaultPage')
    login_url = url_for('renderLoginPage')
    logout_url = url_for('gdisconnect')
    logged_in = 'username' in login_session

    # gets all the categories
    category_rows = session.query(Category).all()
    # gets the last ten items added
    item_rows = session.query(Item).order_by(Item.id.desc()).limit(10).all()

    # put together necessary information for category navigation
    categories = []
    for cat_row in category_rows:
        cat = {}
        cat['name'] = cat_row.name
        cat['url'] = url_for('renderCategoryPage', category_id=cat_row.id)

        categories.append(cat)

    # put together necessary information for latest items navigation
    latest_items = []
    for item_row in item_rows:
        item = {}
        item['name'] = item_row.name
        item['url'] = url_for('renderItemPage', item_id=item_row.id)
        item['category'] = session.query(Category).filter_by(
            id=item_row.category_id).one().name

        latest_items.append(item)

    # put together necessary information to show logged in user
    user = {}
    if logged_in:
        user_id = getUserID(login_session['email'])
        user_row = getUser(user_id)
        user['name'] = user_row.name
        user['picture'] = user_row.picture

    additem_url = url_for('renderAddItemPage')

    # render home page
    return render_template('default.html',
                           user=user,
                           categories=categories,
                           latest_items=latest_items,
                           additem_url=additem_url,
                           homepage_url=homepage_url,
                           login_url=login_url,
                           logout_url=logout_url,
                           logged_in=logged_in)


@app.route('/notauthorized')
def renderNotAuthorizedPage():
    login_url = url_for('renderLoginPage')
    logout_url = url_for('gdisconnect')
    homepage_url = url_for('renderDefaultPage')

    logged_in = 'username' in login_session

    # put together necessary information to show logged in user
    user = {}
    if logged_in:
        user_id = getUserID(login_session['email'])
        user_row = getUser(user_id)
        user['name'] = user_row.name
        user['picture'] = user_row.picture

    return render_template('notauthorized.html',
                           logged_in=logged_in,
                           login_url=login_url,
                           logout_url=logout_url,
                           homepage_url=homepage_url,
                           user=user)


@app.route('/login')
def renderLoginPage():
    homepage_url = url_for('renderDefaultPage')
    login_url = url_for('renderLoginPage')
    logout_url = url_for('gdisconnect')
    logged_in = 'username' in login_session

    state = ''.join(random.choice(string.digits + string.ascii_letters)
                    for x in xrange(32))
    login_session['state'] = state

    # put together necessary information to show logged in user
    user = {}
    if logged_in:
        user_id = getUserID(login_session['email'])
        user_row = getUser(user_id)
        user['name'] = user_row.name
        user['picture'] = user_row.picture

    return render_template('login.html',
                           state=state,
                           user=user,
                           homepage_url=homepage_url,
                           login_url=login_url,
                           logout_url=logout_url,
                           logged_in=logged_in)


@app.route('/gconnect', methods=['POST'])
def gconnect():
    '''authenticate using google oauth / provide success output'''
    # Validate state token
    if request.args.get('state') != login_session['state']:
        response = make_response(json.dumps('Invalid state parameter.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    # Obtain authorization code
    code = request.data

    try:
        # Upgrade the authorization code into a credentials object
        oauth_flow = flow_from_clientsecrets('client_secrets.json', scope='')
        oauth_flow.redirect_uri = 'postmessage'
        credentials = oauth_flow.step2_exchange(code)
    except FlowExchangeError:
        response = make_response(
            json.dumps('Failed to upgrade the authorization code.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Check that the access token is valid.
    access_token = credentials.access_token
    url = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={}'
    url = url.format(access_token)
    h = httplib2.Http()
    result = json.loads(h.request(url, 'GET')[1])
    # If there was an error in the access token info, abort.
    if result.get('error') is not None:
        response = make_response(json.dumps(result.get('error')), 500)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Verify that the access token is used for the intended user.
    gplus_id = credentials.id_token['sub']
    if result['user_id'] != gplus_id:
        response = make_response(
            json.dumps("Token's user ID doesn't match given user ID."), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Verify that the access token is valid for this app.
    if result['issued_to'] != CLIENT_ID:
        response = make_response(
            json.dumps("Token's client ID does not match app's."), 401)
        print "Token's client ID does not match app's."
        response.headers['Content-Type'] = 'application/json'
        return response

    stored_access_token = login_session.get('access_token')
    stored_gplus_id = login_session.get('gplus_id')
    if stored_access_token is not None and gplus_id == stored_gplus_id:
        response = make_response(
            json.dumps('Current user is already connected.'),
            200)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Store the access token in the session for later use.
    login_session['access_token'] = credentials.access_token
    login_session['gplus_id'] = gplus_id

    # Get user info
    userinfo_url = "https://www.googleapis.com/oauth2/v1/userinfo"
    params = {'access_token': credentials.access_token, 'alt': 'json'}
    answer = requests.get(userinfo_url, params=params)

    data = answer.json()

    login_session['username'] = data['name']
    login_session['picture'] = data['picture']
    login_session['email'] = data['email']

    # see if user exists, if it doesn't make a new one
    user_id = getUserID(login_session['email'])
    if user_id is None:
        user_id = createUser(login_session)
    login_session['user_id'] = user_id

    output = ''
    output += '<h1>Welcome, '
    output += login_session['username']
    output += '!</h1>'
    output += '<img src="'
    output += login_session['picture']
    output += ' " style = "width: 100px; '
    output += 'height: 100px;'
    output += 'border-radius: 150px;'
    output += '-webkit-border-radius: 50px;'
    output += '-moz-border-radius: 50px;"> '
    flash("You are now logged in as %s" % login_session['username'])
    return output


@app.route('/gdisconnect')
def gdisconnect():
    '''disconnect a google oauth connected user / reset login_session'''
    homepage_url = url_for('renderDefaultPage')
    # if user is not logged in and tries to disconnect, return error
    access_token = login_session.get('access_token')
    if access_token is None:
        response = make_response(json.dumps(
            'Current user not connected.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    # start http request to revoke access token
    url = 'https://accounts.google.com/o/oauth2/revoke?token={}'.format(
        login_session['access_token'])
    h = httplib2.Http()
    result = h.request(url, 'GET')[0]
    if result['status'] == '200':
        # reset user's sesssion
        del login_session['access_token']
        del login_session['gplus_id']
        del login_session['username']
        del login_session['email']
        del login_session['picture']

        # redirect to home page
        flash("You are now logged out")
        return redirect(homepage_url)
    else:
        # form error response
        response = make_response(json.dumps(
            'Failed to revoke token for given user.', 400))
        response.headers['Content-Type'] = 'application/json'
        return response


@app.route('/catalog/json')
def renderCatalogJSONPage():
    '''json endpoint for information about all items'''
    item_rows = session.query(Item).all()
    return jsonify([item.serialize for item in item_rows])


@app.route('/category/<int:category_id>')
def renderCategoryPage(category_id):
    '''renders category page'''
    homepage_url = url_for('renderDefaultPage')
    login_url = url_for('renderLoginPage')
    logout_url = url_for('gdisconnect')
    logged_in = 'username' in login_session

    # gets all the categories
    category_rows = session.query(Category).all()
    # gets the current category
    curr_category_row = session.query(
        Category).filter_by(id=int(category_id)).one()
    # gets items within category
    item_rows = session.query(Item).filter_by(
        category_id=int(category_id)).all()

    # put together necessary information for category navigation
    categories = []
    for cat_row in category_rows:
        cat = {}
        cat['name'] = cat_row.name
        cat['url'] = url_for('renderCategoryPage', category_id=cat_row.id)

        categories.append(cat)

    # put together necessary information for category navigation
    items = []
    for item_row in item_rows:
        item = {}
        item['name'] = item_row.name
        item['url'] = url_for('renderItemPage', item_id=item_row.id)

        items.append(item)

    # get number of items
    num_items = len(items)

    # get name of current category
    curr_category = curr_category_row.name

    # put together necessary information to show logged in user
    user = {}
    if logged_in:
        user_id = getUserID(login_session['email'])
        user_row = getUser(user_id)
        user['name'] = user_row.name
        user['picture'] = user_row.picture

    # render item page
    return render_template('category.html',
                           user=user,
                           categories=categories,
                           curr_category=curr_category,
                           items=items,
                           num_items=num_items,
                           homepage_url=homepage_url,
                           login_url=login_url,
                           logout_url=logout_url,
                           logged_in=logged_in)


@app.route('/category/<int:category_id>/json')
def renderCategoryJSONPage(category_id):
    '''json endpoint for information about a specific category'''
    cat_row = session.query(Category).filter_by(id=int(category_id)).one()
    return jsonify(cat_row.serialize)


@app.route('/item/<int:item_id>')
def renderItemPage(item_id):
    '''renders item description page'''
    homepage_url = url_for('renderDefaultPage')
    login_url = url_for('renderLoginPage')
    logout_url = url_for('gdisconnect')
    logged_in = 'username' in login_session

    # gets the relevant item
    item_row = session.query(Item).filter_by(id=int(item_id)).one()

    is_owner = (logged_in and getUserID(
        login_session['email']) == item_row.user_id)

    # get crud urls
    edit_url = url_for('renderEditItemPage', item_id=item_row.id)
    delete_url = url_for('renderDeleteItemPage', item_id=item_row.id)

    # put together info for showing item info
    item = {}
    item['name'] = item_row.name
    item['description'] = item_row.description

    # put together necessary information to show logged in user
    user = {}
    if logged_in:
        user_id = getUserID(login_session['email'])
        user_row = getUser(user_id)
        user['name'] = user_row.name
        user['picture'] = user_row.picture

    # render item page
    return render_template('item.html',
                           user=user,
                           item=item,
                           homepage_url=homepage_url,
                           edit_url=edit_url,
                           delete_url=delete_url,
                           login_url=login_url,
                           logout_url=logout_url,
                           logged_in=logged_in,
                           is_owner=is_owner)


@app.route('/item/<int:item_id>/json')
def renderItemJSONPage(item_id):
    '''json endpoint for information about a specific category'''
    item_row = session.query(Item).filter_by(id=int(item_id)).one()
    return jsonify(item_row.serialize)


@app.route('/item/add', methods=['GET', 'POST'])
def renderAddItemPage():
    '''renders add item description page'''
    homepage_url = url_for('renderDefaultPage')
    login_url = url_for('renderLoginPage')
    logout_url = url_for('gdisconnect')
    logged_in = 'username' in login_session

    if not logged_in:
        return redirect(url_for('renderLoginPage'))

    if request.method == 'POST':
        # get inputted item values
        new_item_name = request.form['name']
        new_item_desc = request.form['description']
        new_item_cat_id = int(request.form['category_id'])
        print login_session

        # create item and commit
        new_item = Item(name=new_item_name,
                        description=new_item_desc,
                        category_id=new_item_cat_id,
                        user_id=getUserID(login_session['email']))

        session.add(new_item)
        session.commit()

        # redirect to homepage
        homepage_url = url_for('renderDefaultPage')
        return redirect(homepage_url)
    else:
        # gets all the categories
        category_rows = session.query(Category).all()

        # put together necessary information for category selection
        categories = []
        for cat_row in category_rows:
            cat = {}
            cat['name'] = cat_row.name
            cat['id'] = cat_row.id

            categories.append(cat)

        # put together necessary information to show logged in user
        user = {}
        if logged_in:
            user_id = getUserID(login_session['email'])
            user_row = getUser(user_id)
            user['name'] = user_row.name
            user['picture'] = user_row.picture

        return render_template('additem.html',
                               user=user,
                               categories=categories,
                               post_url=url_for('renderAddItemPage'),
                               homepage_url=homepage_url,
                               login_url=login_url,
                               logout_url=logout_url,
                               logged_in=logged_in)


@app.route('/item/<int:item_id>/edit', methods=['GET', 'POST'])
def renderEditItemPage(item_id):
    '''renders edit item description page'''
    homepage_url = url_for('renderDefaultPage')
    login_url = url_for('renderLoginPage')
    logout_url = url_for('gdisconnect')
    logged_in = 'username' in login_session

    if not logged_in:
        return redirect(url_for('renderLoginPage'))

    # gets the relevant item
    editing_item = session.query(Item).filter_by(id=item_id).one()

    if getUserID(login_session['email']) != editing_item.user_id:
        notauthorized_url = url_for('renderNotAuthorizedPage')
        return redirect(notauthorized_url)

    if request.method == 'POST':
        # get inputted item values and change old item's values
        editing_item.name = request.form['name']
        editing_item.description = request.form['description']
        editing_item.category_id = int(request.form['category_id'])

        # commit edited item
        session.add(editing_item)
        session.commit()

        # redirect to item page
        homepage_url = url_for('renderItemPage', item_id=editing_item.id)
        return redirect(homepage_url)
    else:
        # gets all the categories
        category_rows = session.query(Category).all()

        # put together necessary information for category selection
        categories = []
        for cat_row in category_rows:
            cat = {}
            cat['name'] = cat_row.name
            cat['id'] = cat_row.id

            categories.append(cat)

        # put together necessary information for viewing current item info
        item = {}
        item['name'] = editing_item.name
        item['description'] = editing_item.description
        item['category_id'] = editing_item.category_id

        # put together necessary information to show logged in user
        user = {}
        if logged_in:
            user_id = getUserID(login_session['email'])
            user_row = getUser(user_id)
            user['name'] = user_row.name
            user['picture'] = user_row.picture

        return render_template('edititem.html',
                               user=user,
                               item=item,
                               categories=categories,
                               post_url=url_for('renderEditItemPage',
                                                item_id=item_id),
                               homepage_url=homepage_url,
                               login_url=login_url,
                               logout_url=logout_url,
                               logged_in=logged_in)


@app.route('/item/<int:item_id>/delete', methods=['GET', 'POST'])
def renderDeleteItemPage(item_id):
    '''renders delete item description page'''
    homepage_url = url_for('renderDefaultPage')
    login_url = url_for('renderLoginPage')
    logout_url = url_for('gdisconnect')
    logged_in = 'username' in login_session

    if not logged_in:
        return redirect(url_for('renderLoginPage'))

    # gets the relevant item
    deleting_item = session.query(Item).filter_by(id=item_id).one()

    if getUserID(login_session['email']) != deleting_item.user_id:
        notauthorized_url = url_for('renderNotAuthorizedPage')
        return redirect(notauthorized_url)

    if request.method == 'POST':
        # commit deleting item
        session.delete(deleting_item)
        session.commit()

        # redirect to home page
        return redirect(homepage_url)
    else:
        # put together necessary information for viewing deleting item info
        item = {}
        item['name'] = deleting_item.name

        item_url = url_for('renderItemPage', item_id=item_id)

        # put together necessary information to show logged in user
        user = {}
        if logged_in:
            user_id = getUserID(login_session['email'])
            user_row = getUser(user_id)
            user['name'] = user_row.name
            user['picture'] = user_row.picture

        return render_template('deleteitem.html',
                               user=user,
                               item=item,
                               post_url=url_for('renderDeleteItemPage',
                                                item_id=item_id),
                               homepage_url=homepage_url,
                               item_url=item_url,
                               login_url=login_url,
                               logout_url=logout_url,
                               logged_in=logged_in)


def createUser(login_session):
    newUser = User(name=login_session['username'], email=login_session[
                   'email'], picture=login_session['picture'])
    session.add(newUser)
    session.commit()
    user = session.query(User).filter_by(email=login_session['email']).one()
    return user.id


def getUser(user_id):
    user = session.query(User).filter_by(id=user_id).one()
    return user


def getUserID(email):
    try:
        user = session.query(User).filter_by(email=email).one()
        return user.id
    except:
        return None


# Run server
if __name__ == '__main__':
    app.debug = True
    app.secret_key = 'super_secret_key'
    app.run(host='0.0.0.0', port=8000)
